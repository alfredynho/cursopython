class Star(object):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Estrella {}".format(self.name)

    def girar(self, porcentaje):
        return "la estrella {} gira {}".format(self.name, porcentaje)


a = Star("Negra")
print(a)
print(a.girar(10))
